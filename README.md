## Instalacja

1. Skopiuj plik **gk-shutter-card.js** do katalogu **/homeassistant-ip/config/www/community/gk-shutter-card** 
2. Wejdź do **Konfiguracja -> Dashboardy -> Zasoby** i kliknij **Dodaj zasób**
3. Wprowadź url **/hacsfiles/gk-shutter-card/gk-shutter-card.js** i typ zasobu **Moduł Javascript**
4. Zrestartuj Home Assistant (**Konfiguracja -> Kontrola Serwera -> Uruchom ponownie**)

## Dodawanie karty

Na Dashboardzie (Przegląd) kliknij 3 kropki w prawym rogu i **Edytuj Dashboard**. Następnie **Dodaj kartę** i wyszukaj **GK Karta Rolet**. Wprowadź konfigurację.

### Klasyczny wygląd:

    type: 'custom:gk-shutter-card'
    entities:
       - entity: entity rolety
         name: Nazwa rolety
         buttons_position: right
         invert_percentage: true
         closeDelayPerc: 25

![classic](./images/classic-view.jpg)

### Alternatywny wygląd

    - type: grid
      columns: 2
      square: false
      cards:
      - type: 'custom:gk-shutter-card'
        alternateView: true
        entities:
          - entity: cover.id_rolety1
            name: Pierwsza roleta
            invert_percentage: true
            buttons_position: right
            closeDelayPerc: 25
      - type: 'custom:gk-shutter-card'
        alternateView: true
        entities:
          - entity: cover.id_rolety2
            name: Druga roleta
            invert_percentage: true
            buttons_position: right
            closeDelayPerc: 25
      ...

![alternate](./images/alternate-view.PNG)


## Kalibracja karty
Po poprawnym wykonaniu kalibracji czasu otwierania/zamykania w sterowniku rolet można przejść do kalibracji karty.

Ustaw pozycję rolet w połowie okna. Jeśli podgląd na obrazku nie bedzię się zgadzał ze stanem rzeczywistym **zwiększ/zmniejsz** procent opóźnienia w ustawieniach karty (**closeDelayPerc**)
